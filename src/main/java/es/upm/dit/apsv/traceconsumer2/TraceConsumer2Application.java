package es.upm.dit.apsv.traceconsumer2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.core.env.Environment;

import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer2.Repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.Repository.TransportationOrderRepository;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.function.Consumer;

@SpringBootApplication
public class TraceConsumer2Application {

	@Autowired
	private TraceRepository traceRepository;

	@Autowired
	private TransportationOrderRepository transportationOrderRepository;

	@Autowired
	private Environment env;

	public static void main(String[] args) {
		SpringApplication.run(TraceConsumer2Application.class, args);
	}

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		// Conectar a Kafka, coger las trazas, ponerles un identificador y guardarlas
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			traceRepository.save(t);

			TransportationOrder result = transportationOrderRepository.findById(t.getTruck()).get();

			if (result != null && result.getSt() == 0) {
				result.setLastDate(t.getLastSeen());
				result.setLastLat(t.getLat());
				result.setLastLong(t.getLng());
				// Cuando la distancia es muy pequeña pues dices que ya ha llegado
				if (result.distanceToDestination() < 10)
					result.setSt(1);
				transportationOrderRepository.save(result);
			}
		};
	}

	@Component
	class DemoCommandLineRunner implements CommandLineRunner {

		@Autowired
		private TransportationOrderRepository torderRepository;

		@Override
		public void run(String... args) throws Exception {

			TransportationOrder t = new TransportationOrder();
			t.setToid("MATRICULA");
			t.setTruck("MATRICULA" + System.currentTimeMillis());
			t.setOriginDate(100000000);
			t.setDstDate(t.getOriginDate() + (1000 * 60 * 12));
			t.setOriginLat(0.0);
			t.setOriginLong(0);
			t.setDstLat(44);
			t.setDstLong(88);
			t.setSt(0);
			torderRepository.save(t);
		}
	}

}
